#-------------------------------------------------
#
# Project created by QtCreator 2013-04-04T23:11:38
#
#-------------------------------------------------

QT       += qml quick quick-private gui-private

CONFIG += plugin

TARGET = freevirtualkeyboardplugin
TEMPLATE = lib


SOURCES += VirtualKeyboardInputContextPlugin.cpp \
    VirtualKeyboardInputContext.cpp \
    DeclarativeInputEngine.cpp

HEADERS += VirtualKeyboardInputContextPlugin.h\
    VirtualKeyboardInputContext.h \
    DeclarativeInputEngine.h


deployment.files = *.qml qmldir jsonpath.js Manrope-Regular.otf
layouts.files = layouts/keyboard_*.json

linux-buildroot-g++ {
    deployment.path = /usr/qml/QtQuick/FreeVirtualKeyboard
    layouts.path = /usr/qml/QtQuick/FreeVirtualKeyboard/layouts
    target.path = /usr/lib/qt/plugins/platforminputcontexts
} else {
    deployment.path = $$[QT_INSTALL_QML]/QtQuick/FreeVirtualKeyboard
    layouts.path = $$[QT_INSTALL_QML]/QtQuick/FreeVirtualKeyboard/layouts
    target.path = $$[QT_INSTALL_PLUGINS]/platforminputcontexts
}


INSTALLS += target \
    layouts \
    deployment

OTHER_FILES += \
    JSONListModel.qml \
    InputPanel.qml \
    KeyButton.qml \
    KeyPopup.qml

RESOURCES += \
    virtualkeyboard.qrc

DISTFILES += \
    keyboard_en_US.json
