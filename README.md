# qml-keyboard

A QML on-screen keyboard.
This is mostly based on [QtFreeVirtualKeyboard](https://github.com/githubuser0xFFFF/QtFreeVirtualKeyboard) by Uwe Kindler, was made Qt6-compatible and was expanded to support different layouts defined in a JSON file using [JSONListModel](https://github.com/kromain/qml-utils) by 
Romain Pokrzywka

## build
### Windows
To create makefile on windows:
C:\Qt\6.1.2\mingw81_64\bin\qmake6  virtualkeyboard.pro
Then to build:
C:\Qt\Tools\mingw810_64\bin\mingw32-make.exe
Then to install:
C:\Qt\Tools\mingw810_64\bin\mingw32-make.exe install

(Paths may vary)

### GNU/Linux
qmake6 ~/git/qml-keyboard/src/virtualkeyboard.pro && make -j12 && sudo make install
